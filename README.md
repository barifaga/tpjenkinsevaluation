# TP evaluation jenkins MS ICD 2020-2021
## Liste des extensions :
> 1. git, gitlab
> 2. docker
> 3. PublishHTML
> 4. SCM
    
## La liste des fichiers créés pour l’élaboration de ce processus et leurs justifications
> 1. Dockerfile : permet de builder les images docker front et back   
> 2. Jenkinsfile : permet de definir le pipeline jenkins  

## Les fichiers Dockerfile créés, avec justifications rédigées des choix de conception

![Dockerfile Front](./images/dockerfileFront.PNG)
![Dockerfile Back](./images/dockerfileBack.PNG)

## Le Jenkinsfile

1. récupérer le code source sur un dépôt Git ;
   > stage('Git Checkout'){  
   >        //git credentialsId: 'AccesGitlab', url: 'https://gitlab.com/barifaga/tpjenkinsevaluation.git'  
   >        checkout scm  
   > }  

2. en fonction du langage, construire un artefact ;
    > stage('Build Docker image sonicast Frontend'){    
    >   sh 'docker build -t registry.gitlab.com/barifaga/tpjenkinsevaluation/meteofront:${BUILD_NUMBER} ./frontend'   
    > }    
            
    > stage('Build Docker image sonicast Backend'){   
    >   sh 'docker build -t registry.gitlab.com/barifaga/tpjenkinsevaluation/meteoback:${BUILD_NUMBER} ./backend'   
    > }    

3. mesurer la qualité du code (avec un linter 1 par exemple) ;
    > En cours ... 


4. générer une image Docker, si possible utilisable.
    > stage('Push Docker Image Frontend'){   
    >          withCredentials([string(credentialsId: 'AccesGitlabPush', variable: 'GitlabPushImage')]) {   
    >              sh "docker login -u barifaga@gmail.com -p ${GitlabPushImage} registry.gitlab.com"   
    >              sh 'docker push registry.gitlab.com/barifaga/tpjenkinsevaluation/meteofront:${BUILD_NUMBER}'   
    >              sh "docker push registry.gitlab.com/barifaga/tpjenkinsevaluation/meteoback:${BUILD_NUMBER}"  
    >       }   

    >   }   

5. Retours et analyse

Parmi les outils d'intégration CI/CD, jenkins se differencie par sa capacité à s'interconnecter avec d'autres outils tels gitlab et github et dockerhub etc. Aussi, jenkins a plusieurs plugins qui lui permettent de s'intégrer à plusieurs outils de developpement.
En plus de gitlab CI, Cet outil me permet desormais de mettre en place des pipelines CI/CD seul ou en complement avec d'autres.

Pour mettre en place une chaine CI/CD avec Jenkins les étapes non exhaustives sont les suivantes :
- Clone du depot (gitlab ou scm)
- Build de ou des image(s)
- couvertures de codes
- Tests unitaires
- publication de rapports
- push sur docker registry
- deploiement en recette ou production
- envoi de mail d'information

Le versionning des artefacts peut être geré par le numero de build de façon automatique.
Les accès SSH doivent être privilégiés.

Le pipeline atteindra la maturité aux furs et à messure et la mise en place doit se faire par étape. 